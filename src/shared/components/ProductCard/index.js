import React, { Component } from 'react';

import styles from './styles.scss';
import { ProductCardContent, ProductCardTitle, ProductCardNav } from '../../containers';

class ProductCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isExpanded: true,
    };

    this.onCaretClick = this.onCaretClick.bind(this);
  }

  onCaretClick() {
    this.setState({ isExpanded: !this.state.isExpanded });
  }

  cleanText(text) {
    return text
      .replace(/�/g, '-') // TODO: Probably best to fix improperly encoded characters serverside, clientside is unreliable
      .split('<br />')
      .filter(f => f) // Handle multiple <br /> in a row
      .map((textChunk, i) => {
        const key = `ProductCard-description-${i}`;
        return (<p key={key}>{textChunk}</p>);
      });
  }

  render() {
    const {
      description,
      nextPath,
      previousPath,
      thumbnail,
      title,
    } = this.props;
    let contentWrapperClassName = styles.contentAndNavContainer;
    if (this.state.isExpanded) contentWrapperClassName += ` ${styles.expanded}`;

    return (
      <div className={styles.ProductCard}>
        <ProductCardTitle
          title={title}
          onCaretClick={this.onCaretClick}
          isExpanded={this.state.isExpanded}
        />
        <div className={contentWrapperClassName} aria-hidden={!this.state.isExpanded}>
          <ProductCardContent
            description={this.cleanText(description)}
            thumbnail={thumbnail}
          />
          <ProductCardNav previousPath={previousPath} nextPath={nextPath} />
        </div>
      </div>
    );
  }
}

export default ProductCard;
