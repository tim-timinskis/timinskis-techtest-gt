import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import styles from './styles.scss';
import ProductCard from './components/ProductCard';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      routes: [],
    };
  }

  componentDidMount() {
    this.updateRoutes();
  }

  updateRoutes() {
    this.setState({ routes: this.generateRoutes() });
  }

  generatePathRouteAndTitle(title) {
    return [title, this.generateRouteName(title)];
  }

  generateRoutes() {
    const content = this.props.data.content;

    return content.map(({ description, thumbnail, title }, i) => {
      const titlePath = `/${this.generateRouteName(title)}`;
      const previousPath = content[i - 1] && this.generatePathRouteAndTitle(content[i - 1].title);
      const nextPath = content[i + 1] && this.generatePathRouteAndTitle(content[i + 1].title);
      const productCardProps = { description, thumbnail, title, previousPath, nextPath };

      return (
        <Route
          key={titlePath}
          exact
          path={titlePath}
          render={routeProps => <ProductCard {...routeProps} {...productCardProps} />}
        />
      );
    });
  }

  generateRouteName(routeTitle) {
    return routeTitle
      .toLowerCase()
      .replace(/[^a-zA-Z0-9 ]/g, '') // Remove all special characters except spaces, letters, and numbers
      .replace(/\s\s+/g, ' ')        // Remove all consecutive spaces
      .replace(/\s+/g, '-', '-');    // Replace all spaces with dashes
  }

  render() {
    return (
      <div className={styles.App}>
        <Redirect from="/" exact to="/what-types-of-ipad-minis-are-available" />
        {this.state.routes.length && this.state.routes}
      </div>
    );
  }
}

export default App;
