import React from 'react';
import styles from './styles.scss';

export default ({ thumbnail, description }) => {
  /*
   * TODO:
   * 1. We should be serving image from hosting (caching + easy to change)
   * 2. We should have a placeholder asset
   * 3. ¯\_(ツ)_/¯
   */
  const thumbnailStyle = {
    backgroundImage: `url('/static/static/media/${thumbnail}'), url('http://placehold.it/215x140')`,
  };

  return (
    <div className={styles.ProductCardContent}>
      <span style={thumbnailStyle} className={styles.thumbnail} />
      <article>
        {description}
      </article>
    </div>
  );
};
