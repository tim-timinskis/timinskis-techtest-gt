import React from 'react';
import { Link } from 'react-router-dom';

import styles from './styles.scss';

export default ({ previousPath, nextPath }) => (
  <div className={styles.ProductCardNav}>
    {previousPath && (
      <span className={styles.previous}>
        <Link to={previousPath[1]}>
          {previousPath[0]}
        </Link>
      </span>
      )}
    {nextPath && (
      <span className={styles.next}>
        <Link to={nextPath[1]}>
          {nextPath[0]}
        </Link>
      </span>
    )}
  </div>
);
