import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import App from '../shared/App';

// Extract preloaded data and clean up the globals
const preloadedData = window.__PRELOADED_DATA__; // eslint-disable-line no-underscore-dangle
delete window.__PRELOADED_DATA__; // eslint-disable-line no-underscore-dangle

render(
  (
    <Router>
      <App data={preloadedData} />
    </Router>
), document.getElementById('app'));
