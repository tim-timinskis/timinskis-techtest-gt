import React from 'react';
import styles from './styles.scss';

export default ({ title, isExpanded, onCaretClick }) => {
  let className = styles.ProductCardTitle;
  if (isExpanded) className += ` ${styles.active}`;

  return (
    <div className={className}>
      {/* TODO: Ideally I'd use an SVG icon here, but a makeshift CSS icon for now */}
      <span className={styles.fileIcon} />
      <h1>{title}</h1>
      <button
        type="button"
        className={styles.caret}
        onClick={onCaretClick}
        aria-label="Toggle product description"
        aria-expanded={isExpanded}
      />
    </div>
  );
};
