import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import App from '../shared/App';

import content from '../assets/content.json';

const template = (markup, preloadedData) =>
  `<html lang="en">
      <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>Gumtree Techtest</title>
          <style>
              body {
                  font-family: Helvetica Neue, Arial, sans-serif;
                  margin: 0;
              }
              html { box-sizing: border-box; }
              *, *:before, *:after { box-sizing: inherit; }
          </style>
      </head>
      <body>
          <div id="app">${markup}</div>
          <script>
            window.__PRELOADED_DATA__ = ${JSON.stringify(preloadedData).replace(/</g, '\\u003c')}
          </script>
          <script src="/static/client.js"></script>
      </body>
  </html>`;


export default () =>
  ({ url }, res) => {
    const markup = renderToString(
      <StaticRouter location={url} context={{}}>
        <App />
      </StaticRouter>,
    );

    res.status(200).send(template(markup, content));
  };
