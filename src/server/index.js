const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackHotServerMiddleware = require('webpack-hot-server-middleware');
const config = require('../../webpack.config.js');

const app = express();
const compiler = webpack(config);

// Use webpack magic to automatically update webpack bundle on change
app.use(webpackDevMiddleware(compiler, {
  publicPath: '/static/',
}));
app.use(webpackHotMiddleware(compiler.compilers.find(({ name }) => name === 'client')));
app.use(webpackHotServerMiddleware(compiler));

app.listen(3000, () => console.log('App listening on port 3000')); // eslint-disable-line no-console
