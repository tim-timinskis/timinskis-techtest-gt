# Gumtree Tech Test - Giedrius Timinskis
## Instructions
1. `npm install`
2. `npm run dev`

App runs on `localhost:3000`

## Features
* Server Side rendering
* Routing
* CSS modules with SCSS support
* Fully responsive(ish) :)

## Future improvements
I've left a bunch of `TODO` comments on the things I would change first. Do a global search and see if they make sense!

* This is a development-only webpack setup. For production we'd need a separate config that:
  * Bundles and minifies JS more efficiently (maybe even code splitting? That would require a wider rework though.)
  * Extracts, prefixes, and minifies CSS (could chunk it if going the code-splitting route)
  * Compresses images if we are serving it from local (Probably wouldn't happen in production though)
* If the app was serving static content (as it is now), service worker caching would give offline support + performancee benefits
* Do data fetching serverside (currently doing it from static content, but would need to be more sophisticated for prod)
* Animate component transitions when changing routes + polish animations
* Test across older browsers
* Write tests (!)

For contact: `giedriustiminskis@gmail.com`
